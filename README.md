# homePage

Repository contains weather application development in ReactJS.

<b>Important!</b><hr>
Application is on developing process. In final version application should showing actual date/time, actual weather conditions and weather forecast for actual week. Also application should be loading background according to actual conditions weather.
<br />
<b>Status:</b><hr>
*  Application fetch json object with weather API (see: https://www.npmjs.com/package/dark-sky-api)
*  Created application layout contains static data (layout style desing  in progress)
*  Interface language: Polish
<br />
<b>Screens</b><hr />
<img src="https://i.ibb.co/vJgpsTR/01.png" alt="01" border="0"><br />
<img src="https://i.ibb.co/PNWyLBv/02.png" alt="02" border="0">