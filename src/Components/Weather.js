import React, { Component } from 'react';
const DarkSkyApi = require('dark-sky-api');
class Weather extends Component {
    constructor(props) {
        super(props);
        this.state= {
            actualConditions:null,
            apiKey :'bd99aca25663088c4c20b05a5e17b519'
        };
        DarkSkyApi.apiKey=this.state.apiKey;
        DarkSkyApi.units = 'si'; // default 'us'
        DarkSkyApi.language = 'pl'; // default 'en'       
        this.getWeek(); 

      }
      getCurrent() {
        DarkSkyApi.loadCurrent()
          .then(result => {
            // let currentCondidions=JSON.parse(result);
            this.setState({actualConditions:result});
            console.log(this.state.actualConditions.ozone);
            console.log(result);
          })

      }
      getWeek() {
        DarkSkyApi.loadForecast()
          .then(result => console.log(result));
      }

    render() {
      return null;
    }
}  
export default Weather;